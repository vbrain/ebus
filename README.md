eBus Fotovoltaica UFSC
======================

System for eBus students usage control.
[http://fotovoltaica.ufsc.br/sistemas/fotov/blog/2017/04/24/onibus-eletrico/](http://fotovoltaica.ufsc.br/sistemas/fotov/blog/2017/04/24/onibus-eletrico/)

Project Structure
=================

The project consists of the following seven modules:

- ebus: root project with common metadata and configuration
- ebus-model
- ebus-repository
- ebus-service
- ebus-ui
- ebus-utils
- ebus-web: main application module

Workflow
========

To compile the entire project, run `mvn clean install` in the root directory.

Other basic workflow steps:

- getting started
- compiling the whole project
  - run `mvn install` in parent project
- developing the application
  - edit code in the ui module
  - run `mvn spring-boot:run` in ui module
  - open [http://localhost:8080](http://localhost:8080)
- client side changes or add-ons
  - edit code/POM in widgetset module
  - run `mvn install` in widgetset module
  - if a new add-on has an embedded theme, run `mvn vaadin:update-theme` in the ui module
- debugging client side code
  - run `mvn vaadin:run-codeserver` in widgetset module
  - activate Super Dev Mode in the debug window of the application
- enabling production mode
  - set "vaadin.servlet.productionMode=true" in application.proeprties file in ui module


Contributions
----
Contributions to the project can be done using pull requests.
You will be asked to sign a contribution agreement after creating the first one.

Copyright 2018-2021 vBrain

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
